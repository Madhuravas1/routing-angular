import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-department-list',
  template: `
        <h2>Departments List</h2>
        <ul>
          <li (click)="onSelect(eachDep)" class="each-dept" [class.selected]="isSelectedId(eachDep)" *ngFor="let eachDep of departemntData">
            <span class="dept-id">{{eachDep.id}}</span>{{eachDep.name}}
          </li>
        </ul>
  `,
  styles: [
    `ul{
      list-style-type:none;
    }
    .dept-id{
      background-color:#9d87f5;
      width:1.5rem;
      height:30px;
      border-radius:4px;
      display:flex;
      align-items:center;
      padding-left:5px;
      margin-right:5px;
    }
    .each-dept{
      background-color:#dad9de;
      margin-bottom:4px;
      width:8rem;
      height:30px;
      border-radius:4px;
      display:flex;
      align-items:center;
      cursor:pointer;
    }
    li:hover{
      background-color:#eae9f0;
    }
    .selected{
      background-color:red;
    }
    `

  ]
})
export class DepartmentListComponent implements OnInit {
  selectedId:any;
  departemntData = [
    { "id": 1, "name": "Javascript" },
    { "id": 2, "name": "Angular" },
    { "id": 3, "name": "ReactJs" },
    { "id": 4, "name": "HTML" },
    { "id": 5, "name": "CSS" }
  ]
  constructor(public router:Router, public activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activeRoute.paramMap.subscribe((params:ParamMap) =>{
      this.selectedId = parseInt(params.get('id'))
    })
  }

  onSelect(department:any){
    // this.router.navigate(['/departments', department.id])
    this.router.navigate([department.id], {relativeTo:this.activeRoute})
  }

  isSelectedId(eachDep:any){
    return this.selectedId === eachDep.id
  }
}
