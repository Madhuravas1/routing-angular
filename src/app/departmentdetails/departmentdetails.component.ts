import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-departmentdetails',
  template: `
    <h2>You selected department with Id: {{departmentId}}</h2>

    <p>
      <button (click)="showOverView()">Show over view</button>
      <button (click)="showContact()">show contact</button>
    </p>
    <router-outlet></router-outlet>

    <p>
      <a (click)="onPreviusBtn()">Previus</a>
      <a (click)="onNextBtn()">Next</a>
    </p>
    <div class="btn-card">
      <button  (click)="onBackBtn()">Back</button>
    </div>
  `,
  styles: [`
    a{
      border:2px solid #97bcf7;
      border-radius:3px;
      margin-right:5px;
      padding:8px;
      cursor:pointer;
    }
    .btn-card{
      margin-top:20px;
    }
  `]
})
export class DepartmentdetailsComponent implements OnInit {
  departmentId: number;

  constructor(public activeRoute: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.activeRoute.paramMap.subscribe((params: ParamMap) => {
      this.departmentId = parseInt(params.get('id'))
    })
  }

  onPreviusBtn() {
    let id = this.departmentId - 1
    this.router.navigate(['/departments', id])
  }

  onNextBtn() {
    let id = this.departmentId + 1
    this.router.navigate(['/departments', id])
  }

  onBackBtn() {
    let selectdId = this.departmentId ? this.departmentId : null;
    // this.router.navigate(['/departments', {id:selectdId}])
    this.router.navigate(['../', { id: selectdId }], { relativeTo: this.activeRoute })
  }

  showOverView(){
    this.router.navigate(['overview'], {relativeTo:this.activeRoute})
  }

  showContact(){
    this.router.navigate(['contact'], {relativeTo:this.activeRoute})
  }

}
